# filesystem

A minimal test for using the boost::filesystem library in an R package

### Installation

You can install the released version of VH from [gitlab](https://gitlab.irstea.fr) with:

``` r
library(devtools)
devtools::install_git('https://gitlab.irstea.fr/bjoern.reineking/filesystem.git')
```

### How I got here
- Install bcp via homebrew: brew install boost-bcp
- Download boost version 1.69
- Extract the part of boost necessary for the serialization library to a local folder called "boost" with 
bcp --boost=/Users/bjoern/Downloads/boost_1_69_0 serialization boost
- Create an R package with Rcpp following the RStudio commands:
   - File->New project->...
- Copied header files from ~/boost/boost to pathtoRpackage/inst/include
- Copied src files from ~/boost/libs/filesystem/src to pathtoRpackage/src/libs/filesystem/src
- Added an .cpp file (rcpp_hello_filesystem.cpp) to src that uses  boost::filesystem
- Created a Makevars file in pathtoRpackage/src to build first the filesystem library and then compile the file rcpp_hello_filesystem.cpp
   - The Makevars file was largely based on that in https://github.com/grantbrown/RcppCAF/blob/master/src/Makevars

- TODO: improve the Makevars file. In Writing R extensions, it is written that: 

"When writing a Makevars file for a package you intend to distribute, take care to ensure that it is not specific to your compiler: flags such as -O2 -Wall -pedantic (and all other -W flags: for the Oracle compilers these are used to pass arguments to compiler phases) are all specific to GCC."

- TODO: add Makevars.win to compile under Windows
- Clean code (but this is really just to get started with using a statically linked BOOST library in an R package)


